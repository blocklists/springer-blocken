# Axel Springer Filter List

I try to block all domains that belong to Axel Springer SE in any way.

Up to now usable for uBlock Origin (https://github.com/gorhill/uBlock)!
To add it, please use: https://gitlab.com/blocklists/springer-blocken/-/raw/main/springer-blocken.txt

## Sources used:

* https://github.com/TheBlockList/Blocklist/blob/master/axel-springer
* https://gist.github.com/quantenProjects/903ca1a9e4f395fafb93b2ff4502b665
* https://github.com/autinerd/anti-axelspringer-hosts/
* and the page https://www.axelspringer.com/de/marken

Thanks to all who contributed ;)
